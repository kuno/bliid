
# bliid service test

FIXME: intro the tests

```json
{
    "baseUrl": "http://bliid.frob.mobi",
    "idPrefix": "bliid-"
}

```

### Cleanup

Before we test the service, let's clean up entries from previous tests.

```yaml
baseUrl: http://127.0.0.1
DELETE: /bliid/bliid.frob.mobi
```

The DELETE should respond with a 204 always.

```yaml
status: 204
```

### Service not registered yet

If we now call to the service, we wouldn't get a response... because Caddy doesn't know about it yet.

```yaml
GET: /
```

```yaml
status: 404
```

### Verify configuration

To check what configuration would be written by a PUT request, you can perform the
same request with TRACE.  The server will parse the request, and echo back the
configuration it would have written to file.

```yaml
TRACE: /bliid/bliid.frob.mobi
baseUrl: http://127.0.0.1
body: '{ "port": 8080, "settings": [ "gzip" ] }'
```

The response should be the full configuration for this service (as it would be saved
in /etc/caddy/config.d).

```yaml
status: 200
body:
  regex: "^bliid.frob.mobi {.*^.*gzip.*^.*proxy / 127.0.0.1:8080 {.*^.*transparent.*^.*}.*^}"
```

### Write configuration

Let's save a configuration for a new backend service.  We can use the wald:test built-in server as a back-end.

```yaml
PUT: /bliid/bliid.frob.mobi
baseUrl: http://127.0.0.1
body: '{ "port": 7668 }'
```

The response should be 204 No Content, and the configuration for this service should
have been saved to /etc/caddy/config.d/bliid.frob.mobi, and Caddy should have been
restarted to use the new configuration.

```yaml
status: 204
```

### Connect to the new service

Let's try connecting to the new service again.

```yaml
GET: /
```

Which should respond with the default wald:test redirect page:

```yaml
status: 200
body:
  regex: Redirecting to.*first test result

```
