
bliid
=====

This is an **unofficial** API to configure [Caddy](https://caddyserver.com/).

Probably a better way to implement this is by writing a [Caddyfile Loader
Plugin](https://github.com/mholt/caddy/wiki/Writing-a-Plugin:-Caddyfile-Loader),
however I don't know [Go](https://www.wikidata.org/wiki/Q37227), and text files can be
easily written in any language :)

And the approach taken here could be extended to add support for configuring other
front-end web servers like [Apache](https://httpd.apache.org/) and
[NGINX](https://www.nginx.com/).


when is this useful?
--------------------

When using caddy as a front-end webserver for applications, where the application will know
what configuration it needs from Caddy, it can ask this API to configure caddy as it needs
for a particular hostname.


security
--------

WARNING!  DO NOT USE FOR PRODUCTION!

Allowing any application on your server to direct traffic to itself is a bad idea if
Caddy is serving multiple applications.  If someone breaks into one of your applications
they will be able to direct traffic from all your other applications to the one they
broke into.

bliid is intended for small hobby hosting scenarios where you have multiple small
applications (side projects) sharing a single front-end Caddy server.


setup
-----

Run bin/bootstrap to set up the initial configuration of Caddy.  If you have a Caddyfile in
/etc/caddy/, it will be overwritten by bliid (obviously a backup copy will be made beforehand).


API
===

The API will be listening at /bliid, you can PUT to it with this format:

    PUT http://127.0.0.1/bliid/fully-qualified-application-domain

The request body should contain any directives which should be included in the caddy
block for the specified domain (e.g. to enable gzip or cors).

So, for example if your app uses the "example.com" domain and is listening
(internally) on port 8080:

    PUT http://127.0.0.1/bliid/example.com

    {
        "port": 8080,
        "settings": [
            "cors",
            "gzip",
            "root /var/www/example.com"
        ]
    }


Which will turn into the following (partial) Caddyfile configuration:

    example.com {
        cors
        gzip
        root /var/www/example.com

        proxy / 127.0.0.1:8080 {
            proxy_header Host {host}
        }
    }

The API will respond with one of these status codes:

- 200 OK, Configuration was updated as specified
- 409 Conflict, Listening port already in use for a different domain

A subsequent PUT to the same URL will overwrite the previous settings, and a DELETE request
will delete them.

Debugging
---------

If you wish to verify the configuration before applying it a TRACE request to the same URL will
output the configuration as specified.  (A TRACE request will not read an existing configuration,
to avoid leaking information between applications).

So, a TRACE request like this:

    TRACE http://127.0.0.1/bliid/example.com

    { "port": 8080 }


Will give this response:

    example.com {
        proxy / 127.0.0.1:8080 {
            proxy_header Host {host}
        }
    }


License
=======

Copyright 2018  Kuno Woudt <kuno@frob.nl>

This program is free software: you can redistribute it and/or modify
it under the terms of copyleft-next 0.3.1.  See
[copyleft-next-0.3.1.txt](copyleft-next-0.3.1.txt).

