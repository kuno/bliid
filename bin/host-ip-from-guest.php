#!/usr/bin/env php
<?php
/**
 *   This file is part of bliid - an unofficial configuration API for the Caddy web server.
 *   Copyright (C) 2018  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

function defaultGatewayInHex() {
    $lines = file("/proc/net/route");

    foreach ($lines as $line) {
        $parts = preg_split('/\s+/', $line);
        if (count($parts) < 4) {
            continue;
        }
        if ($parts[0] === 'Iface') {
            continue;
        }
        if ($parts[1] !== '00000000') {
            continue;
        }

        return $parts[2];
    }

    return null;
}

function hexIpToDotNotation($ip) {
    return implode(".", array_map(function ($octet) {
        return hexdec($octet);
    }, array_reverse(str_split($ip, 2))));
}

$gw = defaultGatewayInHex();
if (empty($gw)) {
    error_log("ERROR: Default gateway not found");
    exit(1);
}

echo hexIpToDotNotation($gw) . "\n";

