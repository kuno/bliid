<?php
/**
 *   This file is part of bliid - an unofficial configuration API for the Caddy web server.
 *   Copyright (C) 2018  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

require_once(dirname(__FILE__) . "/../lib/util.php");

function find_pid($name) {
    $warnings = [];

    $pidfile = "/var/run/{$name}.pid";
    $pid = @file_get_contents($pidfile);
    if (!empty($pid)) {
        msg("info", "Found PID for {$name}: {$pid}");
        return (int) $pid;
    }

    $warnings[] = "{$pidfile} not found";

    $dir = @opendir("/proc");
    if (empty($dir)) {
        msg("warning", "could not open /proc");
        return false;
    }

    while (($file = @readdir($dir)) !== false) {
        if (is_numeric($file)) {
            $cmdfile = "/proc/" . $file . "/cmdline";
            $cmdline = @file_get_contents($cmdfile);

            $parts = explode(" ", $cmdline);
            $parts = explode("\0", $parts[0]);

            $command = basename($parts[0]);
            if ($command === $name) {
                return (int) $file;
            }
        }
    }

    $warnings[] = "{$name} process not found";

    foreach ($warnings as $w) {
        // only warn here, so that we don't spit out any warnings if we found Caddy eventually
        msg("warning", $w);
    }

    return false;
}

/**
 * Usage: signal_process("USR1", "caddy");
 */
function signal_process($signal, $processName) {
    $pid = find_pid($processName);
    if (empty($pid)) {
        return false;
    }

    return `kill -{$signal} {$pid}`;
}

function caddy_graceful() {
    signal_process("USR1", "caddy");
}


