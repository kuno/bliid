<?php
/**
 *   This file is part of bliid - an unofficial configuration API for the Caddy web server.
 *   Copyright (C) 2018  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

$verbose = php_sapi_name() === 'cli';
$verbose = true;

function msg($priority, $message) {
    global $verbose;

    if (!$verbose) {
        return;
    }

    $newline = (php_sapi_name() === 'cli') ? "\n" : "<br />\n";

    echo strtoupper($priority) . ": " . $message . $newline;
}

function ends_with($haystack, $needle) {
    $part = substr($haystack, - strlen($needle));

    return $part === $needle;
}

function path_join() {
    $parts = array();

    foreach (func_get_args() as $arg) {
        if ($arg !== '') {
            $parts[] = $arg;
        }
    }

    return preg_replace(',/+,', '/', join('/', $parts));
}

function resolve_path($path)
{
    if ($path[0] !== '/') {
        // we cannot resolve '..' if the parent name is unknown, so let's just give up
        // if the provided path is not absolute.
        return $path;
    }

    $path = str_replace('//', '/', $path);
    $parts = explode('/', $path);
    $out = array();
    foreach ($parts as $part){
        if ($part == '.') continue;
        if ($part == '..') {
            array_pop($out);
            continue;
        }
        $out[] = $part;
    }
    return implode('/', $out);
}
