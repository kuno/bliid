<?php
/**
 *   This file is part of bliid - an unofficial configuration API for the Caddy web server.
 *   Copyright (C) 2018  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

function config() {
    $cfg = new StdClass;

    $bliidConfig = "/etc/bliid/bliid.conf";

    if (is_readable($bliidConfig)) {
        $ini = parse_ini_file($bliidConfig);

        foreach ($ini as $key => $val) {
            $cfg->$key = $val;
        }
    }

    if (empty($cfg->caddy_config_dir)) {
        $cfg->caddy_config_dir = '/etc/caddy/config.d/';
    }

    if (empty($cfg->caddy_log_dir)) {
        $cfg->caddy_log_dir = '/var/log/caddy/';
    }

    if (empty($cfg->web_user)) {
        $cfg->web_user = 'www-data';
    }

    if (empty($cfg->web_group)) {
        $cfg->web_group = 'www-data';
    }

    if (empty($cfg->fpm_sock)) {
        $cfg->fpm_sock = '/var/run/php-fpm/www.sock';
    }

    if (empty($cfg->bliid_root)) {
        $cfg->bliid_root = dirname(__FILE__) . '/../';
    }

    return $cfg;
}

function caddyConfigDirInstructions($cfg) {
    echo "\n";
    echo "Please create it:\n";
    echo "\n";
    echo "    sudo mkdir " . $cfg->caddy_config_dir . "\n";
    echo "    sudo chown " . $cfg->web_user . ":" . $cfg->web_group . " " . $cfg->caddy_config_dir . "\n";
    echo "    sudo chmod 0755 " . $cfg->caddy_config_dir . "\n";
    echo "\n";
    exit(1);
}

function caddyConfigDirDoesNotExist($cfg) {
    echo "ERROR: The bliid/caddy configuration directory does not exist.\n";
    caddyConfigDirInstructions($cfg);
}

function caddyConfigDirIncorrectPermissions($cfg) {
    echo "ERROR: The bliid/caddy configuration directory is not writeable by " . $cfg->web_user . ".\n";
    caddyConfigDirInstructions($cfg);
}

function caddyLogDirInstructions($cfg) {
    echo "\n";
    echo "Please create it:\n";
    echo "\n";
    echo "    sudo mkdir " . $cfg->caddy_log_dir . "\n";
    echo "    sudo chown " . $cfg->web_user . ":" . $cfg->web_group . " " . $cfg->caddy_log_dir . "\n";
    echo "    sudo chmod 0750 " . $cfg->caddy_log_dir . "\n";
    echo "\n";
    exit(1);
}

function caddyLogDirDoesNotExist($cfg) {
    echo "ERROR: The caddy log directory does not exist.\n";
    caddyLogDirInstructions($cfg);
}

function caddyLogDirIncorrectPermissions($cfg) {
    echo "ERROR: The caddy log directory is not writeable by " . $cfg->web_user . ".\n";
    caddyLogDirInstructions($cfg);
}


function verifySetup() {
    $cfg = config();

    if (!is_dir($cfg->caddy_config_dir)) {
        // config directory doesn't exist, try to create it
        if (!@mkdir($cfg->caddy_config_dir)) {
            caddyConfigDirDoesNotExist($cfg);
        }

        if (!@chown($cfg->caddy_config_dir, $cfg->web_user)) {
            caddyConfigDirDoesNotExist($cfg);
        }

        if (!@chgrp($cfg->caddy_config_dir, $cfg->web_group)) {
            caddyConfigDirDoesNotExist($cfg);
        }

        if (!@chmod($cfg->caddy_config_dir, 0755)) {
            caddyConfigDirDoesNotExist($cfg);
        }

        echo "Created bliid/caddy configuration directory: OK\n";
    } else {
        echo "The bliid/caddy configuration directory exists: OK\n";

        // FIXME: check permissions
    }

    if (!is_dir($cfg->caddy_log_dir)) {
        // config directory doesn't exist, try to create it
        if (!@mkdir($cfg->caddy_log_dir)) {
            caddyLogDirDoesNotExist($cfg);
        }

        if (!@chown($cfg->caddy_log_dir, $cfg->web_user)) {
            caddyLogDirDoesNotExist($cfg);
        }

        if (!@chgrp($cfg->caddy_log_dir, $cfg->web_group)) {
            caddyLogDirDoesNotExist($cfg);
        }

        if (!@chmod($cfg->caddy_log_dir, 0750)) {
            caddyLogDirDoesNotExist($cfg);
        }

        echo "Created caddy log directory: OK\n";
    } else {
        echo "The caddy log directory exists: OK\n";

        // FIXME: check permissions
    }
}

function printConfig() {
    $cfg = config();

    echo "bliid setup, using configuration:\n\n";

    $values = get_object_vars($cfg);
    ksort($values);

    $max_len = 0;
    foreach ($values as $key => $val) {
        if (strlen($key) > $max_len) {
            $max_len = strlen($key);
        }
    }

    foreach ($values as $key => $val) {
        echo "    " . str_pad($key, $max_len) . " = " . $val . "\n";
    }

    echo "\n";
}
