<?php

$debug = false;

require_once(dirname(__FILE__) . "/../lib/service.php");
require_once(dirname(__FILE__) . "/../lib/signal.php");
require_once(dirname(__FILE__) . "/../lib/util.php");

function init() {
    $pid = find_pid("caddy");

    $parts = explode("/", trim($_SERVER["REQUEST_URI"], '/'));

    $hostname = array_pop($parts);
    $method = $_SERVER["REQUEST_METHOD"];
    $body = file_get_contents('php://input');

    if ($debug) {
        $ret = compact('pid', 'hostname', 'method', 'body');
        print_r($ret);
    }

    return compact('pid', 'hostname', 'method', 'body');
}

function handle_delete($env) {
    deleteService($env['hostname']);

    writeCaddyfile();
    caddy_graceful();

    http_response_code(204);
}

function parse_request($env) {
    $data = json_decode($env['body'], true);
    $port = false;
    $settings = [];

    if (!empty($data["port"])) {
        $port = (int) $data["port"];
    }

    if (!empty($data["settings"])) {
        $settings = $data["settings"];
    }

    return serviceConfiguration($env['hostname'], $settings, $port);
}

function handle_put($env) {
    saveService($env['hostname'], parse_request($env));

    writeCaddyfile();
    caddy_graceful();

    http_response_code(204);
}

function handle_trace($env) {
    echo parse_request($env);
}

function display_debug_info($env) {
    global $debug;

    if (empty($debug)) {
        return;
    }

    extract($env);

    msg("info", "Method: $method");
    msg("info", "Hostname: $hostname");

    if (empty($pid)) {
        msg("warning", "Caddy not running");
    } else {
        msg("info", "Caddy running as pid $pid");
    }

    // phpinfo();
}

$env = init();

switch($env['method']) {
case 'DELETE':
    handle_delete($env);
    break;
case 'PUT':
case 'POST':
    handle_put($env);
    break;
case 'TRACE':
    handle_trace($env);
    break;
case 'GET':
case 'HEAD':
default:
    http_response_code(204);
    break;
}

/* display_debug_info($env); */
